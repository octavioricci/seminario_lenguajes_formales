# Generated from conjuntos.g4 by ANTLR 4.7.1
# encoding: utf-8
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys

def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3!")
        buf.write("\u00ca\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7")
        buf.write("\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r\4\16")
        buf.write("\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22\4\23\t\23")
        buf.write("\4\24\t\24\4\25\t\25\3\2\6\2,\n\2\r\2\16\2-\3\3\3\3\3")
        buf.write("\3\3\3\3\3\3\3\3\3\3\3\5\38\n\3\3\4\3\4\3\4\3\4\3\4\3")
        buf.write("\4\3\4\3\4\5\4B\n\4\3\5\3\5\3\5\3\5\3\5\3\6\3\6\3\6\3")
        buf.write("\6\3\6\3\7\3\7\3\7\3\7\3\7\3\b\3\b\3\b\3\b\3\b\3\t\3\t")
        buf.write("\3\t\3\t\3\t\3\n\3\n\3\n\3\13\3\13\3\13\3\f\3\f\3\f\3")
        buf.write("\r\3\r\3\r\3\r\3\r\3\16\3\16\3\16\3\16\3\16\3\16\3\16")
        buf.write("\3\16\3\16\5\16t\n\16\3\17\3\17\3\17\3\17\6\17z\n\17\r")
        buf.write("\17\16\17{\3\17\3\17\6\17\u0080\n\17\r\17\16\17\u0081")
        buf.write("\5\17\u0084\n\17\3\17\3\17\3\20\3\20\3\20\3\20\6\20\u008c")
        buf.write("\n\20\r\20\16\20\u008d\3\20\3\20\3\21\3\21\3\21\3\21\3")
        buf.write("\21\3\21\3\21\3\21\3\21\3\21\3\21\7\21\u009d\n\21\f\21")
        buf.write("\16\21\u00a0\13\21\3\22\3\22\5\22\u00a4\n\22\3\23\3\23")
        buf.write("\3\23\3\23\3\23\3\23\3\23\3\23\3\23\7\23\u00af\n\23\f")
        buf.write("\23\16\23\u00b2\13\23\3\24\3\24\3\24\3\24\3\24\3\24\3")
        buf.write("\24\3\24\3\24\7\24\u00bd\n\24\f\24\16\24\u00c0\13\24\3")
        buf.write("\25\3\25\3\25\3\25\3\25\3\25\5\25\u00c8\n\25\3\25\2\5")
        buf.write(" $&\26\2\4\6\b\n\f\16\20\22\24\26\30\32\34\36 \"$&(\2")
        buf.write("\2\2\u00d3\2+\3\2\2\2\4\67\3\2\2\2\6A\3\2\2\2\bC\3\2\2")
        buf.write("\2\nH\3\2\2\2\fM\3\2\2\2\16R\3\2\2\2\20W\3\2\2\2\22\\")
        buf.write("\3\2\2\2\24_\3\2\2\2\26b\3\2\2\2\30e\3\2\2\2\32s\3\2\2")
        buf.write("\2\34u\3\2\2\2\36\u0087\3\2\2\2 \u0091\3\2\2\2\"\u00a3")
        buf.write("\3\2\2\2$\u00a5\3\2\2\2&\u00b3\3\2\2\2(\u00c7\3\2\2\2")
        buf.write("*,\5\4\3\2+*\3\2\2\2,-\3\2\2\2-+\3\2\2\2-.\3\2\2\2.\3")
        buf.write("\3\2\2\2/8\5$\23\2\608\5\32\16\2\618\7\36\2\2\628\5 \21")
        buf.write("\2\638\5\34\17\2\648\5\36\20\2\658\5\30\r\2\668\5\6\4")
        buf.write("\2\67/\3\2\2\2\67\60\3\2\2\2\67\61\3\2\2\2\67\62\3\2\2")
        buf.write("\2\67\63\3\2\2\2\67\64\3\2\2\2\67\65\3\2\2\2\67\66\3\2")
        buf.write("\2\28\5\3\2\2\29B\5\b\5\2:B\5\n\6\2;B\5\f\7\2<B\5\16\b")
        buf.write("\2=B\5\20\t\2>B\5\22\n\2?B\5\24\13\2@B\5\26\f\2A9\3\2")
        buf.write("\2\2A:\3\2\2\2A;\3\2\2\2A<\3\2\2\2A=\3\2\2\2A>\3\2\2\2")
        buf.write("A?\3\2\2\2A@\3\2\2\2B\7\3\2\2\2CD\7\36\2\2DE\7\3\2\2E")
        buf.write("F\7\36\2\2FG\7\4\2\2G\t\3\2\2\2HI\7\36\2\2IJ\7\5\2\2J")
        buf.write("K\7\36\2\2KL\7\4\2\2L\13\3\2\2\2MN\7\36\2\2NO\7\6\2\2")
        buf.write("OP\7\36\2\2PQ\7\4\2\2Q\r\3\2\2\2RS\7\36\2\2ST\7\7\2\2")
        buf.write("TU\7\36\2\2UV\7\4\2\2V\17\3\2\2\2WX\7\36\2\2XY\7\b\2\2")
        buf.write("YZ\7\37\2\2Z[\7\4\2\2[\21\3\2\2\2\\]\7\36\2\2]^\7\t\2")
        buf.write("\2^\23\3\2\2\2_`\7\36\2\2`a\7\n\2\2a\25\3\2\2\2bc\7\36")
        buf.write("\2\2cd\7\13\2\2d\27\3\2\2\2ef\7\f\2\2fg\7\37\2\2gh\7\37")
        buf.write("\2\2hi\7\r\2\2i\31\3\2\2\2jk\7\36\2\2kl\7\16\2\2lt\5$")
        buf.write("\23\2mn\7\36\2\2no\7\16\2\2ot\5\30\r\2pq\7\36\2\2qr\7")
        buf.write("\16\2\2rt\5\6\4\2sj\3\2\2\2sm\3\2\2\2sp\3\2\2\2t\33\3")
        buf.write("\2\2\2uv\7\17\2\2vw\5 \21\2wy\7\20\2\2xz\5\4\3\2yx\3\2")
        buf.write("\2\2z{\3\2\2\2{y\3\2\2\2{|\3\2\2\2|\u0083\3\2\2\2}\177")
        buf.write("\7\21\2\2~\u0080\5\4\3\2\177~\3\2\2\2\u0080\u0081\3\2")
        buf.write("\2\2\u0081\177\3\2\2\2\u0081\u0082\3\2\2\2\u0082\u0084")
        buf.write("\3\2\2\2\u0083}\3\2\2\2\u0083\u0084\3\2\2\2\u0084\u0085")
        buf.write("\3\2\2\2\u0085\u0086\7\22\2\2\u0086\35\3\2\2\2\u0087\u0088")
        buf.write("\7\23\2\2\u0088\u0089\5 \21\2\u0089\u008b\7\24\2\2\u008a")
        buf.write("\u008c\5\4\3\2\u008b\u008a\3\2\2\2\u008c\u008d\3\2\2\2")
        buf.write("\u008d\u008b\3\2\2\2\u008d\u008e\3\2\2\2\u008e\u008f\3")
        buf.write("\2\2\2\u008f\u0090\7\25\2\2\u0090\37\3\2\2\2\u0091\u0092")
        buf.write("\b\21\1\2\u0092\u0093\5\"\22\2\u0093\u0094\7\35\2\2\u0094")
        buf.write("\u0095\5\"\22\2\u0095\u009e\3\2\2\2\u0096\u0097\f\4\2")
        buf.write("\2\u0097\u0098\7\34\2\2\u0098\u009d\5 \21\5\u0099\u009a")
        buf.write("\f\3\2\2\u009a\u009b\7\33\2\2\u009b\u009d\5 \21\4\u009c")
        buf.write("\u0096\3\2\2\2\u009c\u0099\3\2\2\2\u009d\u00a0\3\2\2\2")
        buf.write("\u009e\u009c\3\2\2\2\u009e\u009f\3\2\2\2\u009f!\3\2\2")
        buf.write("\2\u00a0\u009e\3\2\2\2\u00a1\u00a4\5$\23\2\u00a2\u00a4")
        buf.write("\7\36\2\2\u00a3\u00a1\3\2\2\2\u00a3\u00a2\3\2\2\2\u00a4")
        buf.write("#\3\2\2\2\u00a5\u00a6\b\23\1\2\u00a6\u00a7\5&\24\2\u00a7")
        buf.write("\u00b0\3\2\2\2\u00a8\u00a9\f\4\2\2\u00a9\u00aa\7\26\2")
        buf.write("\2\u00aa\u00af\5&\24\2\u00ab\u00ac\f\3\2\2\u00ac\u00ad")
        buf.write("\7\27\2\2\u00ad\u00af\5&\24\2\u00ae\u00a8\3\2\2\2\u00ae")
        buf.write("\u00ab\3\2\2\2\u00af\u00b2\3\2\2\2\u00b0\u00ae\3\2\2\2")
        buf.write("\u00b0\u00b1\3\2\2\2\u00b1%\3\2\2\2\u00b2\u00b0\3\2\2")
        buf.write("\2\u00b3\u00b4\b\24\1\2\u00b4\u00b5\5(\25\2\u00b5\u00be")
        buf.write("\3\2\2\2\u00b6\u00b7\f\4\2\2\u00b7\u00b8\7\30\2\2\u00b8")
        buf.write("\u00bd\5(\25\2\u00b9\u00ba\f\3\2\2\u00ba\u00bb\7\31\2")
        buf.write("\2\u00bb\u00bd\5(\25\2\u00bc\u00b6\3\2\2\2\u00bc\u00b9")
        buf.write("\3\2\2\2\u00bd\u00c0\3\2\2\2\u00be\u00bc\3\2\2\2\u00be")
        buf.write("\u00bf\3\2\2\2\u00bf\'\3\2\2\2\u00c0\u00be\3\2\2\2\u00c1")
        buf.write("\u00c8\7\37\2\2\u00c2\u00c8\7\36\2\2\u00c3\u00c4\7\32")
        buf.write("\2\2\u00c4\u00c5\5$\23\2\u00c5\u00c6\7\4\2\2\u00c6\u00c8")
        buf.write("\3\2\2\2\u00c7\u00c1\3\2\2\2\u00c7\u00c2\3\2\2\2\u00c7")
        buf.write("\u00c3\3\2\2\2\u00c8)\3\2\2\2\22-\67As{\u0081\u0083\u008d")
        buf.write("\u009c\u009e\u00a3\u00ae\u00b0\u00bc\u00be\u00c7")
        return buf.getvalue()


class conjuntosParser ( Parser ):

    grammarFileName = "conjuntos.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "'.intersection('", "')'", "'.union('", 
                     "'.complement('", "'.difference('", "'.belongs('", 
                     "'.sum()'", "'.average()'", "'.length()'", "'set['", 
                     "']'", "'='", "'if'", "'then'", "'else'", "'fi'", "'while'", 
                     "'do'", "'done'", "'+'", "'-'", "'*'", "'/'", "'('", 
                     "'or'", "'and'" ]

    symbolicNames = [ "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "OR_OPERATOR", "AND_OPERATOR", "COMPARATOR_OPERATOR", 
                      "VARNAME", "NUMBER", "DIGIT", "WHITESPACE" ]

    RULE_program = 0
    RULE_statement = 1
    RULE_setConjuntos = 2
    RULE_setIntersection = 3
    RULE_setUnion = 4
    RULE_setComplement = 5
    RULE_setDifference = 6
    RULE_setBelongs = 7
    RULE_setSum = 8
    RULE_setAverage = 9
    RULE_setLength = 10
    RULE_setNumber = 11
    RULE_assignStatement = 12
    RULE_ifStatement = 13
    RULE_whileStatement = 14
    RULE_booleanExpression = 15
    RULE_operand = 16
    RULE_expression = 17
    RULE_term = 18
    RULE_factor = 19

    ruleNames =  [ "program", "statement", "setConjuntos", "setIntersection", 
                   "setUnion", "setComplement", "setDifference", "setBelongs", 
                   "setSum", "setAverage", "setLength", "setNumber", "assignStatement", 
                   "ifStatement", "whileStatement", "booleanExpression", 
                   "operand", "expression", "term", "factor" ]

    EOF = Token.EOF
    T__0=1
    T__1=2
    T__2=3
    T__3=4
    T__4=5
    T__5=6
    T__6=7
    T__7=8
    T__8=9
    T__9=10
    T__10=11
    T__11=12
    T__12=13
    T__13=14
    T__14=15
    T__15=16
    T__16=17
    T__17=18
    T__18=19
    T__19=20
    T__20=21
    T__21=22
    T__22=23
    T__23=24
    OR_OPERATOR=25
    AND_OPERATOR=26
    COMPARATOR_OPERATOR=27
    VARNAME=28
    NUMBER=29
    DIGIT=30
    WHITESPACE=31

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7.1")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None



    class ProgramContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def statement(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(conjuntosParser.StatementContext)
            else:
                return self.getTypedRuleContext(conjuntosParser.StatementContext,i)


        def getRuleIndex(self):
            return conjuntosParser.RULE_program

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterProgram" ):
                listener.enterProgram(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitProgram" ):
                listener.exitProgram(self)




    def program(self):

        localctx = conjuntosParser.ProgramContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_program)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 41 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 40
                self.statement()
                self.state = 43 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not ((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << conjuntosParser.T__9) | (1 << conjuntosParser.T__12) | (1 << conjuntosParser.T__16) | (1 << conjuntosParser.T__23) | (1 << conjuntosParser.VARNAME) | (1 << conjuntosParser.NUMBER))) != 0)):
                    break

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class StatementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def expression(self):
            return self.getTypedRuleContext(conjuntosParser.ExpressionContext,0)


        def assignStatement(self):
            return self.getTypedRuleContext(conjuntosParser.AssignStatementContext,0)


        def VARNAME(self):
            return self.getToken(conjuntosParser.VARNAME, 0)

        def booleanExpression(self):
            return self.getTypedRuleContext(conjuntosParser.BooleanExpressionContext,0)


        def ifStatement(self):
            return self.getTypedRuleContext(conjuntosParser.IfStatementContext,0)


        def whileStatement(self):
            return self.getTypedRuleContext(conjuntosParser.WhileStatementContext,0)


        def setNumber(self):
            return self.getTypedRuleContext(conjuntosParser.SetNumberContext,0)


        def setConjuntos(self):
            return self.getTypedRuleContext(conjuntosParser.SetConjuntosContext,0)


        def getRuleIndex(self):
            return conjuntosParser.RULE_statement

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterStatement" ):
                listener.enterStatement(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitStatement" ):
                listener.exitStatement(self)




    def statement(self):

        localctx = conjuntosParser.StatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_statement)
        try:
            self.state = 53
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,1,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 45
                self.expression(0)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 46
                self.assignStatement()
                pass

            elif la_ == 3:
                self.enterOuterAlt(localctx, 3)
                self.state = 47
                self.match(conjuntosParser.VARNAME)
                pass

            elif la_ == 4:
                self.enterOuterAlt(localctx, 4)
                self.state = 48
                self.booleanExpression(0)
                pass

            elif la_ == 5:
                self.enterOuterAlt(localctx, 5)
                self.state = 49
                self.ifStatement()
                pass

            elif la_ == 6:
                self.enterOuterAlt(localctx, 6)
                self.state = 50
                self.whileStatement()
                pass

            elif la_ == 7:
                self.enterOuterAlt(localctx, 7)
                self.state = 51
                self.setNumber()
                pass

            elif la_ == 8:
                self.enterOuterAlt(localctx, 8)
                self.state = 52
                self.setConjuntos()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class SetConjuntosContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def setIntersection(self):
            return self.getTypedRuleContext(conjuntosParser.SetIntersectionContext,0)


        def setUnion(self):
            return self.getTypedRuleContext(conjuntosParser.SetUnionContext,0)


        def setComplement(self):
            return self.getTypedRuleContext(conjuntosParser.SetComplementContext,0)


        def setDifference(self):
            return self.getTypedRuleContext(conjuntosParser.SetDifferenceContext,0)


        def setBelongs(self):
            return self.getTypedRuleContext(conjuntosParser.SetBelongsContext,0)


        def setSum(self):
            return self.getTypedRuleContext(conjuntosParser.SetSumContext,0)


        def setAverage(self):
            return self.getTypedRuleContext(conjuntosParser.SetAverageContext,0)


        def setLength(self):
            return self.getTypedRuleContext(conjuntosParser.SetLengthContext,0)


        def getRuleIndex(self):
            return conjuntosParser.RULE_setConjuntos

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterSetConjuntos" ):
                listener.enterSetConjuntos(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitSetConjuntos" ):
                listener.exitSetConjuntos(self)




    def setConjuntos(self):

        localctx = conjuntosParser.SetConjuntosContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_setConjuntos)
        try:
            self.state = 63
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,2,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 55
                self.setIntersection()
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 56
                self.setUnion()
                pass

            elif la_ == 3:
                self.enterOuterAlt(localctx, 3)
                self.state = 57
                self.setComplement()
                pass

            elif la_ == 4:
                self.enterOuterAlt(localctx, 4)
                self.state = 58
                self.setDifference()
                pass

            elif la_ == 5:
                self.enterOuterAlt(localctx, 5)
                self.state = 59
                self.setBelongs()
                pass

            elif la_ == 6:
                self.enterOuterAlt(localctx, 6)
                self.state = 60
                self.setSum()
                pass

            elif la_ == 7:
                self.enterOuterAlt(localctx, 7)
                self.state = 61
                self.setAverage()
                pass

            elif la_ == 8:
                self.enterOuterAlt(localctx, 8)
                self.state = 62
                self.setLength()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class SetIntersectionContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.name = None # Token
            self.name2 = None # Token

        def VARNAME(self, i:int=None):
            if i is None:
                return self.getTokens(conjuntosParser.VARNAME)
            else:
                return self.getToken(conjuntosParser.VARNAME, i)

        def getRuleIndex(self):
            return conjuntosParser.RULE_setIntersection

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterSetIntersection" ):
                listener.enterSetIntersection(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitSetIntersection" ):
                listener.exitSetIntersection(self)




    def setIntersection(self):

        localctx = conjuntosParser.SetIntersectionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_setIntersection)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 65
            localctx.name = self.match(conjuntosParser.VARNAME)
            self.state = 66
            self.match(conjuntosParser.T__0)
            self.state = 67
            localctx.name2 = self.match(conjuntosParser.VARNAME)
            self.state = 68
            self.match(conjuntosParser.T__1)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class SetUnionContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.name = None # Token
            self.name2 = None # Token

        def VARNAME(self, i:int=None):
            if i is None:
                return self.getTokens(conjuntosParser.VARNAME)
            else:
                return self.getToken(conjuntosParser.VARNAME, i)

        def getRuleIndex(self):
            return conjuntosParser.RULE_setUnion

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterSetUnion" ):
                listener.enterSetUnion(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitSetUnion" ):
                listener.exitSetUnion(self)




    def setUnion(self):

        localctx = conjuntosParser.SetUnionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_setUnion)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 70
            localctx.name = self.match(conjuntosParser.VARNAME)
            self.state = 71
            self.match(conjuntosParser.T__2)
            self.state = 72
            localctx.name2 = self.match(conjuntosParser.VARNAME)
            self.state = 73
            self.match(conjuntosParser.T__1)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class SetComplementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.name = None # Token
            self.name2 = None # Token

        def VARNAME(self, i:int=None):
            if i is None:
                return self.getTokens(conjuntosParser.VARNAME)
            else:
                return self.getToken(conjuntosParser.VARNAME, i)

        def getRuleIndex(self):
            return conjuntosParser.RULE_setComplement

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterSetComplement" ):
                listener.enterSetComplement(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitSetComplement" ):
                listener.exitSetComplement(self)




    def setComplement(self):

        localctx = conjuntosParser.SetComplementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 10, self.RULE_setComplement)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 75
            localctx.name = self.match(conjuntosParser.VARNAME)
            self.state = 76
            self.match(conjuntosParser.T__3)
            self.state = 77
            localctx.name2 = self.match(conjuntosParser.VARNAME)
            self.state = 78
            self.match(conjuntosParser.T__1)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class SetDifferenceContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.name = None # Token
            self.name2 = None # Token

        def VARNAME(self, i:int=None):
            if i is None:
                return self.getTokens(conjuntosParser.VARNAME)
            else:
                return self.getToken(conjuntosParser.VARNAME, i)

        def getRuleIndex(self):
            return conjuntosParser.RULE_setDifference

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterSetDifference" ):
                listener.enterSetDifference(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitSetDifference" ):
                listener.exitSetDifference(self)




    def setDifference(self):

        localctx = conjuntosParser.SetDifferenceContext(self, self._ctx, self.state)
        self.enterRule(localctx, 12, self.RULE_setDifference)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 80
            localctx.name = self.match(conjuntosParser.VARNAME)
            self.state = 81
            self.match(conjuntosParser.T__4)
            self.state = 82
            localctx.name2 = self.match(conjuntosParser.VARNAME)
            self.state = 83
            self.match(conjuntosParser.T__1)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class SetBelongsContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.name = None # Token
            self.name2 = None # Token

        def VARNAME(self):
            return self.getToken(conjuntosParser.VARNAME, 0)

        def NUMBER(self):
            return self.getToken(conjuntosParser.NUMBER, 0)

        def getRuleIndex(self):
            return conjuntosParser.RULE_setBelongs

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterSetBelongs" ):
                listener.enterSetBelongs(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitSetBelongs" ):
                listener.exitSetBelongs(self)




    def setBelongs(self):

        localctx = conjuntosParser.SetBelongsContext(self, self._ctx, self.state)
        self.enterRule(localctx, 14, self.RULE_setBelongs)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 85
            localctx.name = self.match(conjuntosParser.VARNAME)
            self.state = 86
            self.match(conjuntosParser.T__5)
            self.state = 87
            localctx.name2 = self.match(conjuntosParser.NUMBER)
            self.state = 88
            self.match(conjuntosParser.T__1)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class SetSumContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.name = None # Token

        def VARNAME(self):
            return self.getToken(conjuntosParser.VARNAME, 0)

        def getRuleIndex(self):
            return conjuntosParser.RULE_setSum

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterSetSum" ):
                listener.enterSetSum(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitSetSum" ):
                listener.exitSetSum(self)




    def setSum(self):

        localctx = conjuntosParser.SetSumContext(self, self._ctx, self.state)
        self.enterRule(localctx, 16, self.RULE_setSum)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 90
            localctx.name = self.match(conjuntosParser.VARNAME)
            self.state = 91
            self.match(conjuntosParser.T__6)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class SetAverageContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.name = None # Token

        def VARNAME(self):
            return self.getToken(conjuntosParser.VARNAME, 0)

        def getRuleIndex(self):
            return conjuntosParser.RULE_setAverage

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterSetAverage" ):
                listener.enterSetAverage(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitSetAverage" ):
                listener.exitSetAverage(self)




    def setAverage(self):

        localctx = conjuntosParser.SetAverageContext(self, self._ctx, self.state)
        self.enterRule(localctx, 18, self.RULE_setAverage)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 93
            localctx.name = self.match(conjuntosParser.VARNAME)
            self.state = 94
            self.match(conjuntosParser.T__7)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class SetLengthContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.name = None # Token

        def VARNAME(self):
            return self.getToken(conjuntosParser.VARNAME, 0)

        def getRuleIndex(self):
            return conjuntosParser.RULE_setLength

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterSetLength" ):
                listener.enterSetLength(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitSetLength" ):
                listener.exitSetLength(self)




    def setLength(self):

        localctx = conjuntosParser.SetLengthContext(self, self._ctx, self.state)
        self.enterRule(localctx, 20, self.RULE_setLength)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 96
            localctx.name = self.match(conjuntosParser.VARNAME)
            self.state = 97
            self.match(conjuntosParser.T__8)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class SetNumberContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.start = None # Token
            self.end = None # Token

        def NUMBER(self, i:int=None):
            if i is None:
                return self.getTokens(conjuntosParser.NUMBER)
            else:
                return self.getToken(conjuntosParser.NUMBER, i)

        def getRuleIndex(self):
            return conjuntosParser.RULE_setNumber

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterSetNumber" ):
                listener.enterSetNumber(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitSetNumber" ):
                listener.exitSetNumber(self)




    def setNumber(self):

        localctx = conjuntosParser.SetNumberContext(self, self._ctx, self.state)
        self.enterRule(localctx, 22, self.RULE_setNumber)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 99
            self.match(conjuntosParser.T__9)
            self.state = 100
            localctx.start = self.match(conjuntosParser.NUMBER)
            self.state = 101
            localctx.end = self.match(conjuntosParser.NUMBER)
            self.state = 102
            self.match(conjuntosParser.T__10)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class AssignStatementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def VARNAME(self):
            return self.getToken(conjuntosParser.VARNAME, 0)

        def expression(self):
            return self.getTypedRuleContext(conjuntosParser.ExpressionContext,0)


        def setNumber(self):
            return self.getTypedRuleContext(conjuntosParser.SetNumberContext,0)


        def setConjuntos(self):
            return self.getTypedRuleContext(conjuntosParser.SetConjuntosContext,0)


        def getRuleIndex(self):
            return conjuntosParser.RULE_assignStatement

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAssignStatement" ):
                listener.enterAssignStatement(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAssignStatement" ):
                listener.exitAssignStatement(self)




    def assignStatement(self):

        localctx = conjuntosParser.AssignStatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 24, self.RULE_assignStatement)
        try:
            self.state = 113
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,3,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 104
                self.match(conjuntosParser.VARNAME)
                self.state = 105
                self.match(conjuntosParser.T__11)
                self.state = 106
                self.expression(0)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 107
                self.match(conjuntosParser.VARNAME)
                self.state = 108
                self.match(conjuntosParser.T__11)
                self.state = 109
                self.setNumber()
                pass

            elif la_ == 3:
                self.enterOuterAlt(localctx, 3)
                self.state = 110
                self.match(conjuntosParser.VARNAME)
                self.state = 111
                self.match(conjuntosParser.T__11)
                self.state = 112
                self.setConjuntos()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class IfStatementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def booleanExpression(self):
            return self.getTypedRuleContext(conjuntosParser.BooleanExpressionContext,0)


        def statement(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(conjuntosParser.StatementContext)
            else:
                return self.getTypedRuleContext(conjuntosParser.StatementContext,i)


        def getRuleIndex(self):
            return conjuntosParser.RULE_ifStatement

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterIfStatement" ):
                listener.enterIfStatement(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitIfStatement" ):
                listener.exitIfStatement(self)




    def ifStatement(self):

        localctx = conjuntosParser.IfStatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 26, self.RULE_ifStatement)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 115
            self.match(conjuntosParser.T__12)
            self.state = 116
            self.booleanExpression(0)
            self.state = 117
            self.match(conjuntosParser.T__13)
            self.state = 119 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 118
                self.statement()
                self.state = 121 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not ((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << conjuntosParser.T__9) | (1 << conjuntosParser.T__12) | (1 << conjuntosParser.T__16) | (1 << conjuntosParser.T__23) | (1 << conjuntosParser.VARNAME) | (1 << conjuntosParser.NUMBER))) != 0)):
                    break

            self.state = 129
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==conjuntosParser.T__14:
                self.state = 123
                self.match(conjuntosParser.T__14)
                self.state = 125 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while True:
                    self.state = 124
                    self.statement()
                    self.state = 127 
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    if not ((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << conjuntosParser.T__9) | (1 << conjuntosParser.T__12) | (1 << conjuntosParser.T__16) | (1 << conjuntosParser.T__23) | (1 << conjuntosParser.VARNAME) | (1 << conjuntosParser.NUMBER))) != 0)):
                        break



            self.state = 131
            self.match(conjuntosParser.T__15)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class WhileStatementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def booleanExpression(self):
            return self.getTypedRuleContext(conjuntosParser.BooleanExpressionContext,0)


        def statement(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(conjuntosParser.StatementContext)
            else:
                return self.getTypedRuleContext(conjuntosParser.StatementContext,i)


        def getRuleIndex(self):
            return conjuntosParser.RULE_whileStatement

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterWhileStatement" ):
                listener.enterWhileStatement(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitWhileStatement" ):
                listener.exitWhileStatement(self)




    def whileStatement(self):

        localctx = conjuntosParser.WhileStatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 28, self.RULE_whileStatement)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 133
            self.match(conjuntosParser.T__16)
            self.state = 134
            self.booleanExpression(0)
            self.state = 135
            self.match(conjuntosParser.T__17)
            self.state = 137 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 136
                self.statement()
                self.state = 139 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not ((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << conjuntosParser.T__9) | (1 << conjuntosParser.T__12) | (1 << conjuntosParser.T__16) | (1 << conjuntosParser.T__23) | (1 << conjuntosParser.VARNAME) | (1 << conjuntosParser.NUMBER))) != 0)):
                    break

            self.state = 141
            self.match(conjuntosParser.T__18)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class BooleanExpressionContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.op = None # Token

        def operand(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(conjuntosParser.OperandContext)
            else:
                return self.getTypedRuleContext(conjuntosParser.OperandContext,i)


        def COMPARATOR_OPERATOR(self):
            return self.getToken(conjuntosParser.COMPARATOR_OPERATOR, 0)

        def booleanExpression(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(conjuntosParser.BooleanExpressionContext)
            else:
                return self.getTypedRuleContext(conjuntosParser.BooleanExpressionContext,i)


        def AND_OPERATOR(self):
            return self.getToken(conjuntosParser.AND_OPERATOR, 0)

        def OR_OPERATOR(self):
            return self.getToken(conjuntosParser.OR_OPERATOR, 0)

        def getRuleIndex(self):
            return conjuntosParser.RULE_booleanExpression

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterBooleanExpression" ):
                listener.enterBooleanExpression(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitBooleanExpression" ):
                listener.exitBooleanExpression(self)



    def booleanExpression(self, _p:int=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = conjuntosParser.BooleanExpressionContext(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 30
        self.enterRecursionRule(localctx, 30, self.RULE_booleanExpression, _p)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 144
            self.operand()
            self.state = 145
            localctx.op = self.match(conjuntosParser.COMPARATOR_OPERATOR)
            self.state = 146
            self.operand()
            self._ctx.stop = self._input.LT(-1)
            self.state = 156
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,9,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    self.state = 154
                    self._errHandler.sync(self)
                    la_ = self._interp.adaptivePredict(self._input,8,self._ctx)
                    if la_ == 1:
                        localctx = conjuntosParser.BooleanExpressionContext(self, _parentctx, _parentState)
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_booleanExpression)
                        self.state = 148
                        if not self.precpred(self._ctx, 2):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 2)")
                        self.state = 149
                        localctx.op = self.match(conjuntosParser.AND_OPERATOR)
                        self.state = 150
                        self.booleanExpression(3)
                        pass

                    elif la_ == 2:
                        localctx = conjuntosParser.BooleanExpressionContext(self, _parentctx, _parentState)
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_booleanExpression)
                        self.state = 151
                        if not self.precpred(self._ctx, 1):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 1)")
                        self.state = 152
                        localctx.op = self.match(conjuntosParser.OR_OPERATOR)
                        self.state = 153
                        self.booleanExpression(2)
                        pass

             
                self.state = 158
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,9,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx

    class OperandContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def expression(self):
            return self.getTypedRuleContext(conjuntosParser.ExpressionContext,0)


        def VARNAME(self):
            return self.getToken(conjuntosParser.VARNAME, 0)

        def getRuleIndex(self):
            return conjuntosParser.RULE_operand

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterOperand" ):
                listener.enterOperand(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitOperand" ):
                listener.exitOperand(self)




    def operand(self):

        localctx = conjuntosParser.OperandContext(self, self._ctx, self.state)
        self.enterRule(localctx, 32, self.RULE_operand)
        try:
            self.state = 161
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,10,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 159
                self.expression(0)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 160
                self.match(conjuntosParser.VARNAME)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class ExpressionContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def term(self):
            return self.getTypedRuleContext(conjuntosParser.TermContext,0)


        def expression(self):
            return self.getTypedRuleContext(conjuntosParser.ExpressionContext,0)


        def getRuleIndex(self):
            return conjuntosParser.RULE_expression

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterExpression" ):
                listener.enterExpression(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitExpression" ):
                listener.exitExpression(self)



    def expression(self, _p:int=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = conjuntosParser.ExpressionContext(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 34
        self.enterRecursionRule(localctx, 34, self.RULE_expression, _p)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 164
            self.term(0)
            self._ctx.stop = self._input.LT(-1)
            self.state = 174
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,12,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    self.state = 172
                    self._errHandler.sync(self)
                    la_ = self._interp.adaptivePredict(self._input,11,self._ctx)
                    if la_ == 1:
                        localctx = conjuntosParser.ExpressionContext(self, _parentctx, _parentState)
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expression)
                        self.state = 166
                        if not self.precpred(self._ctx, 2):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 2)")
                        self.state = 167
                        self.match(conjuntosParser.T__19)
                        self.state = 168
                        self.term(0)
                        pass

                    elif la_ == 2:
                        localctx = conjuntosParser.ExpressionContext(self, _parentctx, _parentState)
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expression)
                        self.state = 169
                        if not self.precpred(self._ctx, 1):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 1)")
                        self.state = 170
                        self.match(conjuntosParser.T__20)
                        self.state = 171
                        self.term(0)
                        pass

             
                self.state = 176
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,12,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx

    class TermContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def factor(self):
            return self.getTypedRuleContext(conjuntosParser.FactorContext,0)


        def term(self):
            return self.getTypedRuleContext(conjuntosParser.TermContext,0)


        def getRuleIndex(self):
            return conjuntosParser.RULE_term

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTerm" ):
                listener.enterTerm(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTerm" ):
                listener.exitTerm(self)



    def term(self, _p:int=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = conjuntosParser.TermContext(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 36
        self.enterRecursionRule(localctx, 36, self.RULE_term, _p)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 178
            self.factor()
            self._ctx.stop = self._input.LT(-1)
            self.state = 188
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,14,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    self.state = 186
                    self._errHandler.sync(self)
                    la_ = self._interp.adaptivePredict(self._input,13,self._ctx)
                    if la_ == 1:
                        localctx = conjuntosParser.TermContext(self, _parentctx, _parentState)
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_term)
                        self.state = 180
                        if not self.precpred(self._ctx, 2):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 2)")
                        self.state = 181
                        self.match(conjuntosParser.T__21)
                        self.state = 182
                        self.factor()
                        pass

                    elif la_ == 2:
                        localctx = conjuntosParser.TermContext(self, _parentctx, _parentState)
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_term)
                        self.state = 183
                        if not self.precpred(self._ctx, 1):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 1)")
                        self.state = 184
                        self.match(conjuntosParser.T__22)
                        self.state = 185
                        self.factor()
                        pass

             
                self.state = 190
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,14,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx

    class FactorContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.n = None # Token
            self.vn = None # Token

        def NUMBER(self):
            return self.getToken(conjuntosParser.NUMBER, 0)

        def VARNAME(self):
            return self.getToken(conjuntosParser.VARNAME, 0)

        def expression(self):
            return self.getTypedRuleContext(conjuntosParser.ExpressionContext,0)


        def getRuleIndex(self):
            return conjuntosParser.RULE_factor

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterFactor" ):
                listener.enterFactor(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitFactor" ):
                listener.exitFactor(self)




    def factor(self):

        localctx = conjuntosParser.FactorContext(self, self._ctx, self.state)
        self.enterRule(localctx, 38, self.RULE_factor)
        try:
            self.state = 197
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [conjuntosParser.NUMBER]:
                self.enterOuterAlt(localctx, 1)
                self.state = 191
                localctx.n = self.match(conjuntosParser.NUMBER)
                pass
            elif token in [conjuntosParser.VARNAME]:
                self.enterOuterAlt(localctx, 2)
                self.state = 192
                localctx.vn = self.match(conjuntosParser.VARNAME)
                pass
            elif token in [conjuntosParser.T__23]:
                self.enterOuterAlt(localctx, 3)
                self.state = 193
                self.match(conjuntosParser.T__23)
                self.state = 194
                self.expression(0)
                self.state = 195
                self.match(conjuntosParser.T__1)
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx



    def sempred(self, localctx:RuleContext, ruleIndex:int, predIndex:int):
        if self._predicates == None:
            self._predicates = dict()
        self._predicates[15] = self.booleanExpression_sempred
        self._predicates[17] = self.expression_sempred
        self._predicates[18] = self.term_sempred
        pred = self._predicates.get(ruleIndex, None)
        if pred is None:
            raise Exception("No predicate with index:" + str(ruleIndex))
        else:
            return pred(localctx, predIndex)

    def booleanExpression_sempred(self, localctx:BooleanExpressionContext, predIndex:int):
            if predIndex == 0:
                return self.precpred(self._ctx, 2)
         

            if predIndex == 1:
                return self.precpred(self._ctx, 1)
         

    def expression_sempred(self, localctx:ExpressionContext, predIndex:int):
            if predIndex == 2:
                return self.precpred(self._ctx, 2)
         

            if predIndex == 3:
                return self.precpred(self._ctx, 1)
         

    def term_sempred(self, localctx:TermContext, predIndex:int):
            if predIndex == 4:
                return self.precpred(self._ctx, 2)
         

            if predIndex == 5:
                return self.precpred(self._ctx, 1)
         




