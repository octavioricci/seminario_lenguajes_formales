# TP 1
##### Alumno: Octavio Ricci
##### Legago: 68607
## Fecha de entrega: 11-Octubre-2018 
Realizar la gramática para operar con conjuntos.

## Conjunto  
Establecer la forma de crear conjunto numericos.  
Establecer el conjunto de referencia (o universal)


## Ejemplo:  
A = conjunto[0..5]      //Define el conjunto {0,1,2,3,4,5}  
B = conjunto[0..10, 2]  //Define el conjunto {0,2,4,6,8,10}

## Operaciones:  
Intersección  
Unión  
Diferencia   
Complemento  
Asignación  
Mostrar  
Pertenece Elemento  
Sumar Elementos  
Promediar Elementos  
Longitud  


## Ejemplos: 
A = set[1,10,2]     // {1,3,5,7,9}  
A.belong(3)         // Return True  
C = A.intersect(B) o  
C = A inter B  
A                   // Muestra el conjunto  
res = A.sum  