# Generated from conjuntos.g4 by ANTLR 4.7.1
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys


def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2!")
        buf.write("\u00ea\b\1\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7")
        buf.write("\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r")
        buf.write("\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22\4\23")
        buf.write("\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30")
        buf.write("\4\31\t\31\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36")
        buf.write("\t\36\4\37\t\37\4 \t \3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2")
        buf.write("\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\3\3\3\3\4\3\4\3\4\3\4\3")
        buf.write("\4\3\4\3\4\3\4\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5")
        buf.write("\3\5\3\5\3\5\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3")
        buf.write("\6\3\6\3\6\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\b")
        buf.write("\3\b\3\b\3\b\3\b\3\b\3\b\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3")
        buf.write("\t\3\t\3\t\3\t\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n")
        buf.write("\3\13\3\13\3\13\3\13\3\13\3\f\3\f\3\r\3\r\3\16\3\16\3")
        buf.write("\16\3\17\3\17\3\17\3\17\3\17\3\20\3\20\3\20\3\20\3\20")
        buf.write("\3\21\3\21\3\21\3\22\3\22\3\22\3\22\3\22\3\22\3\23\3\23")
        buf.write("\3\23\3\24\3\24\3\24\3\24\3\24\3\25\3\25\3\26\3\26\3\27")
        buf.write("\3\27\3\30\3\30\3\31\3\31\3\32\3\32\3\32\3\33\3\33\3\33")
        buf.write("\3\33\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34\5\34")
        buf.write("\u00dc\n\34\3\35\3\35\3\36\6\36\u00e1\n\36\r\36\16\36")
        buf.write("\u00e2\3\37\3\37\3 \3 \3 \3 \2\2!\3\3\5\4\7\5\t\6\13\7")
        buf.write("\r\b\17\t\21\n\23\13\25\f\27\r\31\16\33\17\35\20\37\21")
        buf.write("!\22#\23%\24\'\25)\26+\27-\30/\31\61\32\63\33\65\34\67")
        buf.write("\359\36;\37= ?!\3\2\6\4\2>>@@\3\2c|\3\2\62;\5\2\13\f\17")
        buf.write("\17\"\"\2\u00ee\2\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2\2\2\2")
        buf.write("\t\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21")
        buf.write("\3\2\2\2\2\23\3\2\2\2\2\25\3\2\2\2\2\27\3\2\2\2\2\31\3")
        buf.write("\2\2\2\2\33\3\2\2\2\2\35\3\2\2\2\2\37\3\2\2\2\2!\3\2\2")
        buf.write("\2\2#\3\2\2\2\2%\3\2\2\2\2\'\3\2\2\2\2)\3\2\2\2\2+\3\2")
        buf.write("\2\2\2-\3\2\2\2\2/\3\2\2\2\2\61\3\2\2\2\2\63\3\2\2\2\2")
        buf.write("\65\3\2\2\2\2\67\3\2\2\2\29\3\2\2\2\2;\3\2\2\2\2=\3\2")
        buf.write("\2\2\2?\3\2\2\2\3A\3\2\2\2\5P\3\2\2\2\7R\3\2\2\2\tZ\3")
        buf.write("\2\2\2\13g\3\2\2\2\rt\3\2\2\2\17~\3\2\2\2\21\u0085\3\2")
        buf.write("\2\2\23\u0090\3\2\2\2\25\u009a\3\2\2\2\27\u009f\3\2\2")
        buf.write("\2\31\u00a1\3\2\2\2\33\u00a3\3\2\2\2\35\u00a6\3\2\2\2")
        buf.write("\37\u00ab\3\2\2\2!\u00b0\3\2\2\2#\u00b3\3\2\2\2%\u00b9")
        buf.write("\3\2\2\2\'\u00bc\3\2\2\2)\u00c1\3\2\2\2+\u00c3\3\2\2\2")
        buf.write("-\u00c5\3\2\2\2/\u00c7\3\2\2\2\61\u00c9\3\2\2\2\63\u00cb")
        buf.write("\3\2\2\2\65\u00ce\3\2\2\2\67\u00db\3\2\2\29\u00dd\3\2")
        buf.write("\2\2;\u00e0\3\2\2\2=\u00e4\3\2\2\2?\u00e6\3\2\2\2AB\7")
        buf.write("\60\2\2BC\7k\2\2CD\7p\2\2DE\7v\2\2EF\7g\2\2FG\7t\2\2G")
        buf.write("H\7u\2\2HI\7g\2\2IJ\7e\2\2JK\7v\2\2KL\7k\2\2LM\7q\2\2")
        buf.write("MN\7p\2\2NO\7*\2\2O\4\3\2\2\2PQ\7+\2\2Q\6\3\2\2\2RS\7")
        buf.write("\60\2\2ST\7w\2\2TU\7p\2\2UV\7k\2\2VW\7q\2\2WX\7p\2\2X")
        buf.write("Y\7*\2\2Y\b\3\2\2\2Z[\7\60\2\2[\\\7e\2\2\\]\7q\2\2]^\7")
        buf.write("o\2\2^_\7r\2\2_`\7n\2\2`a\7g\2\2ab\7o\2\2bc\7g\2\2cd\7")
        buf.write("p\2\2de\7v\2\2ef\7*\2\2f\n\3\2\2\2gh\7\60\2\2hi\7f\2\2")
        buf.write("ij\7k\2\2jk\7h\2\2kl\7h\2\2lm\7g\2\2mn\7t\2\2no\7g\2\2")
        buf.write("op\7p\2\2pq\7e\2\2qr\7g\2\2rs\7*\2\2s\f\3\2\2\2tu\7\60")
        buf.write("\2\2uv\7d\2\2vw\7g\2\2wx\7n\2\2xy\7q\2\2yz\7p\2\2z{\7")
        buf.write("i\2\2{|\7u\2\2|}\7*\2\2}\16\3\2\2\2~\177\7\60\2\2\177")
        buf.write("\u0080\7u\2\2\u0080\u0081\7w\2\2\u0081\u0082\7o\2\2\u0082")
        buf.write("\u0083\7*\2\2\u0083\u0084\7+\2\2\u0084\20\3\2\2\2\u0085")
        buf.write("\u0086\7\60\2\2\u0086\u0087\7c\2\2\u0087\u0088\7x\2\2")
        buf.write("\u0088\u0089\7g\2\2\u0089\u008a\7t\2\2\u008a\u008b\7c")
        buf.write("\2\2\u008b\u008c\7i\2\2\u008c\u008d\7g\2\2\u008d\u008e")
        buf.write("\7*\2\2\u008e\u008f\7+\2\2\u008f\22\3\2\2\2\u0090\u0091")
        buf.write("\7\60\2\2\u0091\u0092\7n\2\2\u0092\u0093\7g\2\2\u0093")
        buf.write("\u0094\7p\2\2\u0094\u0095\7i\2\2\u0095\u0096\7v\2\2\u0096")
        buf.write("\u0097\7j\2\2\u0097\u0098\7*\2\2\u0098\u0099\7+\2\2\u0099")
        buf.write("\24\3\2\2\2\u009a\u009b\7u\2\2\u009b\u009c\7g\2\2\u009c")
        buf.write("\u009d\7v\2\2\u009d\u009e\7]\2\2\u009e\26\3\2\2\2\u009f")
        buf.write("\u00a0\7_\2\2\u00a0\30\3\2\2\2\u00a1\u00a2\7?\2\2\u00a2")
        buf.write("\32\3\2\2\2\u00a3\u00a4\7k\2\2\u00a4\u00a5\7h\2\2\u00a5")
        buf.write("\34\3\2\2\2\u00a6\u00a7\7v\2\2\u00a7\u00a8\7j\2\2\u00a8")
        buf.write("\u00a9\7g\2\2\u00a9\u00aa\7p\2\2\u00aa\36\3\2\2\2\u00ab")
        buf.write("\u00ac\7g\2\2\u00ac\u00ad\7n\2\2\u00ad\u00ae\7u\2\2\u00ae")
        buf.write("\u00af\7g\2\2\u00af \3\2\2\2\u00b0\u00b1\7h\2\2\u00b1")
        buf.write("\u00b2\7k\2\2\u00b2\"\3\2\2\2\u00b3\u00b4\7y\2\2\u00b4")
        buf.write("\u00b5\7j\2\2\u00b5\u00b6\7k\2\2\u00b6\u00b7\7n\2\2\u00b7")
        buf.write("\u00b8\7g\2\2\u00b8$\3\2\2\2\u00b9\u00ba\7f\2\2\u00ba")
        buf.write("\u00bb\7q\2\2\u00bb&\3\2\2\2\u00bc\u00bd\7f\2\2\u00bd")
        buf.write("\u00be\7q\2\2\u00be\u00bf\7p\2\2\u00bf\u00c0\7g\2\2\u00c0")
        buf.write("(\3\2\2\2\u00c1\u00c2\7-\2\2\u00c2*\3\2\2\2\u00c3\u00c4")
        buf.write("\7/\2\2\u00c4,\3\2\2\2\u00c5\u00c6\7,\2\2\u00c6.\3\2\2")
        buf.write("\2\u00c7\u00c8\7\61\2\2\u00c8\60\3\2\2\2\u00c9\u00ca\7")
        buf.write("*\2\2\u00ca\62\3\2\2\2\u00cb\u00cc\7q\2\2\u00cc\u00cd")
        buf.write("\7t\2\2\u00cd\64\3\2\2\2\u00ce\u00cf\7c\2\2\u00cf\u00d0")
        buf.write("\7p\2\2\u00d0\u00d1\7f\2\2\u00d1\66\3\2\2\2\u00d2\u00d3")
        buf.write("\7?\2\2\u00d3\u00dc\7?\2\2\u00d4\u00d5\7>\2\2\u00d5\u00dc")
        buf.write("\7?\2\2\u00d6\u00d7\7?\2\2\u00d7\u00dc\7@\2\2\u00d8\u00dc")
        buf.write("\t\2\2\2\u00d9\u00da\7#\2\2\u00da\u00dc\7?\2\2\u00db\u00d2")
        buf.write("\3\2\2\2\u00db\u00d4\3\2\2\2\u00db\u00d6\3\2\2\2\u00db")
        buf.write("\u00d8\3\2\2\2\u00db\u00d9\3\2\2\2\u00dc8\3\2\2\2\u00dd")
        buf.write("\u00de\t\3\2\2\u00de:\3\2\2\2\u00df\u00e1\5=\37\2\u00e0")
        buf.write("\u00df\3\2\2\2\u00e1\u00e2\3\2\2\2\u00e2\u00e0\3\2\2\2")
        buf.write("\u00e2\u00e3\3\2\2\2\u00e3<\3\2\2\2\u00e4\u00e5\t\4\2")
        buf.write("\2\u00e5>\3\2\2\2\u00e6\u00e7\t\5\2\2\u00e7\u00e8\3\2")
        buf.write("\2\2\u00e8\u00e9\b \2\2\u00e9@\3\2\2\2\5\2\u00db\u00e2")
        buf.write("\3\b\2\2")
        return buf.getvalue()


class conjuntosLexer(Lexer):

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    T__0 = 1
    T__1 = 2
    T__2 = 3
    T__3 = 4
    T__4 = 5
    T__5 = 6
    T__6 = 7
    T__7 = 8
    T__8 = 9
    T__9 = 10
    T__10 = 11
    T__11 = 12
    T__12 = 13
    T__13 = 14
    T__14 = 15
    T__15 = 16
    T__16 = 17
    T__17 = 18
    T__18 = 19
    T__19 = 20
    T__20 = 21
    T__21 = 22
    T__22 = 23
    T__23 = 24
    OR_OPERATOR = 25
    AND_OPERATOR = 26
    COMPARATOR_OPERATOR = 27
    VARNAME = 28
    NUMBER = 29
    DIGIT = 30
    WHITESPACE = 31

    channelNames = [ u"DEFAULT_TOKEN_CHANNEL", u"HIDDEN" ]

    modeNames = [ "DEFAULT_MODE" ]

    literalNames = [ "<INVALID>",
            "'.intersection('", "')'", "'.union('", "'.complement('", "'.difference('", 
            "'.belongs('", "'.sum()'", "'.average()'", "'.length()'", "'set['", 
            "']'", "'='", "'if'", "'then'", "'else'", "'fi'", "'while'", 
            "'do'", "'done'", "'+'", "'-'", "'*'", "'/'", "'('", "'or'", 
            "'and'" ]

    symbolicNames = [ "<INVALID>",
            "OR_OPERATOR", "AND_OPERATOR", "COMPARATOR_OPERATOR", "VARNAME", 
            "NUMBER", "DIGIT", "WHITESPACE" ]

    ruleNames = [ "T__0", "T__1", "T__2", "T__3", "T__4", "T__5", "T__6", 
                  "T__7", "T__8", "T__9", "T__10", "T__11", "T__12", "T__13", 
                  "T__14", "T__15", "T__16", "T__17", "T__18", "T__19", 
                  "T__20", "T__21", "T__22", "T__23", "OR_OPERATOR", "AND_OPERATOR", 
                  "COMPARATOR_OPERATOR", "VARNAME", "NUMBER", "DIGIT", "WHITESPACE" ]

    grammarFileName = "conjuntos.g4"

    def __init__(self, input=None, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7.1")
        self._interp = LexerATNSimulator(self, self.atn, self.decisionsToDFA, PredictionContextCache())
        self._actions = None
        self._predicates = None


