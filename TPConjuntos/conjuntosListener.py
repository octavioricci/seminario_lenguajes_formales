# Generated from conjuntos.g4 by ANTLR 4.7.1
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .conjuntosParser import conjuntosParser
else:
    from conjuntosParser import conjuntosParser

# This class defines a complete listener for a parse tree produced by conjuntosParser.
class conjuntosListener(ParseTreeListener):

    # Enter a parse tree produced by conjuntosParser#program.
    def enterProgram(self, ctx:conjuntosParser.ProgramContext):
        pass

    # Exit a parse tree produced by conjuntosParser#program.
    def exitProgram(self, ctx:conjuntosParser.ProgramContext):
        pass


    # Enter a parse tree produced by conjuntosParser#statement.
    def enterStatement(self, ctx:conjuntosParser.StatementContext):
        pass

    # Exit a parse tree produced by conjuntosParser#statement.
    def exitStatement(self, ctx:conjuntosParser.StatementContext):
        pass


    # Enter a parse tree produced by conjuntosParser#setConjuntos.
    def enterSetConjuntos(self, ctx:conjuntosParser.SetConjuntosContext):
        pass

    # Exit a parse tree produced by conjuntosParser#setConjuntos.
    def exitSetConjuntos(self, ctx:conjuntosParser.SetConjuntosContext):
        pass


    # Enter a parse tree produced by conjuntosParser#setIntersection.
    def enterSetIntersection(self, ctx:conjuntosParser.SetIntersectionContext):
        pass

    # Exit a parse tree produced by conjuntosParser#setIntersection.
    def exitSetIntersection(self, ctx:conjuntosParser.SetIntersectionContext):
        pass


    # Enter a parse tree produced by conjuntosParser#setUnion.
    def enterSetUnion(self, ctx:conjuntosParser.SetUnionContext):
        pass

    # Exit a parse tree produced by conjuntosParser#setUnion.
    def exitSetUnion(self, ctx:conjuntosParser.SetUnionContext):
        pass


    # Enter a parse tree produced by conjuntosParser#setComplement.
    def enterSetComplement(self, ctx:conjuntosParser.SetComplementContext):
        pass

    # Exit a parse tree produced by conjuntosParser#setComplement.
    def exitSetComplement(self, ctx:conjuntosParser.SetComplementContext):
        pass


    # Enter a parse tree produced by conjuntosParser#setDifference.
    def enterSetDifference(self, ctx:conjuntosParser.SetDifferenceContext):
        pass

    # Exit a parse tree produced by conjuntosParser#setDifference.
    def exitSetDifference(self, ctx:conjuntosParser.SetDifferenceContext):
        pass


    # Enter a parse tree produced by conjuntosParser#setBelongs.
    def enterSetBelongs(self, ctx:conjuntosParser.SetBelongsContext):
        pass

    # Exit a parse tree produced by conjuntosParser#setBelongs.
    def exitSetBelongs(self, ctx:conjuntosParser.SetBelongsContext):
        pass


    # Enter a parse tree produced by conjuntosParser#setSum.
    def enterSetSum(self, ctx:conjuntosParser.SetSumContext):
        pass

    # Exit a parse tree produced by conjuntosParser#setSum.
    def exitSetSum(self, ctx:conjuntosParser.SetSumContext):
        pass


    # Enter a parse tree produced by conjuntosParser#setAverage.
    def enterSetAverage(self, ctx:conjuntosParser.SetAverageContext):
        pass

    # Exit a parse tree produced by conjuntosParser#setAverage.
    def exitSetAverage(self, ctx:conjuntosParser.SetAverageContext):
        pass


    # Enter a parse tree produced by conjuntosParser#setLength.
    def enterSetLength(self, ctx:conjuntosParser.SetLengthContext):
        pass

    # Exit a parse tree produced by conjuntosParser#setLength.
    def exitSetLength(self, ctx:conjuntosParser.SetLengthContext):
        pass


    # Enter a parse tree produced by conjuntosParser#setNumber.
    def enterSetNumber(self, ctx:conjuntosParser.SetNumberContext):
        pass

    # Exit a parse tree produced by conjuntosParser#setNumber.
    def exitSetNumber(self, ctx:conjuntosParser.SetNumberContext):
        pass


    # Enter a parse tree produced by conjuntosParser#assignStatement.
    def enterAssignStatement(self, ctx:conjuntosParser.AssignStatementContext):
        pass

    # Exit a parse tree produced by conjuntosParser#assignStatement.
    def exitAssignStatement(self, ctx:conjuntosParser.AssignStatementContext):
        pass


    # Enter a parse tree produced by conjuntosParser#ifStatement.
    def enterIfStatement(self, ctx:conjuntosParser.IfStatementContext):
        pass

    # Exit a parse tree produced by conjuntosParser#ifStatement.
    def exitIfStatement(self, ctx:conjuntosParser.IfStatementContext):
        pass


    # Enter a parse tree produced by conjuntosParser#whileStatement.
    def enterWhileStatement(self, ctx:conjuntosParser.WhileStatementContext):
        pass

    # Exit a parse tree produced by conjuntosParser#whileStatement.
    def exitWhileStatement(self, ctx:conjuntosParser.WhileStatementContext):
        pass


    # Enter a parse tree produced by conjuntosParser#booleanExpression.
    def enterBooleanExpression(self, ctx:conjuntosParser.BooleanExpressionContext):
        pass

    # Exit a parse tree produced by conjuntosParser#booleanExpression.
    def exitBooleanExpression(self, ctx:conjuntosParser.BooleanExpressionContext):
        pass


    # Enter a parse tree produced by conjuntosParser#operand.
    def enterOperand(self, ctx:conjuntosParser.OperandContext):
        pass

    # Exit a parse tree produced by conjuntosParser#operand.
    def exitOperand(self, ctx:conjuntosParser.OperandContext):
        pass


    # Enter a parse tree produced by conjuntosParser#expression.
    def enterExpression(self, ctx:conjuntosParser.ExpressionContext):
        pass

    # Exit a parse tree produced by conjuntosParser#expression.
    def exitExpression(self, ctx:conjuntosParser.ExpressionContext):
        pass


    # Enter a parse tree produced by conjuntosParser#term.
    def enterTerm(self, ctx:conjuntosParser.TermContext):
        pass

    # Exit a parse tree produced by conjuntosParser#term.
    def exitTerm(self, ctx:conjuntosParser.TermContext):
        pass


    # Enter a parse tree produced by conjuntosParser#factor.
    def enterFactor(self, ctx:conjuntosParser.FactorContext):
        pass

    # Exit a parse tree produced by conjuntosParser#factor.
    def exitFactor(self, ctx:conjuntosParser.FactorContext):
        pass


