grammar conjuntos;

program:
    (statement)+;


statement:
      expression                
    | assignStatement  
    | VARNAME                   
    | booleanExpression          
    | ifStatement
    | whileStatement
    | setNumber
    | setConjuntos
    ;

// Genero setConjuntos porque de lo contrario cuando hago
// c = b.intersection(b) nunca entro en visitIntersection, ya que entra primero a expression
// De esta forma lo soluciono

setConjuntos:
    setIntersection
    | setUnion
    | setComplement
    | setDifference
    | setBelongs
    | setSum
    | setAverage
    | setLength;

setIntersection:
    name=VARNAME '.intersection(' name2=VARNAME ')'
    ;

setUnion:
    name=VARNAME '.union(' name2=VARNAME ')';

setComplement:
    name=VARNAME '.complement(' name2=VARNAME ')';

setDifference:
    name=VARNAME '.difference(' name2=VARNAME ')';

setBelongs:
    name=VARNAME '.belongs(' name2=NUMBER ')';

setSum:
    name=VARNAME '.sum()';

setAverage:
    name=VARNAME '.average()';

setLength:
    name=VARNAME '.length()';

setNumber:  'set['start=NUMBER end=NUMBER']';

assignStatement:
      VARNAME '=' expression 
    | VARNAME '=' setNumber
    | VARNAME '=' setConjuntos
    ;

ifStatement:
    'if' booleanExpression
    'then' (statement)+
    ('else' (statement)+)?          // ? Significa que puede o no ir
    'fi';

whileStatement:
    'while' booleanExpression 'do'
        (statement)+
    'done';


booleanExpression:  operand op=COMPARATOR_OPERATOR operand | 
                    booleanExpression op=AND_OPERATOR booleanExpression |
                    booleanExpression op=OR_OPERATOR booleanExpression;

operand: expression | VARNAME;

expression: term | expression '+' term | expression '-' term;

term: factor | term '*' factor | term '/' factor;

factor:
    n=NUMBER | vn=VARNAME | '(' expression ')';

OR_OPERATOR: 'or';

AND_OPERATOR: 'and';

COMPARATOR_OPERATOR: '=='|'<='|'=>'|'<'|'>'|'!=';

VARNAME: [a-z];

NUMBER: DIGIT+;

DIGIT: [0-9];

WHITESPACE: [ \n\r\t]-> skip;