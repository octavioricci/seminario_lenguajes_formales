from antlr4 import CommonTokenStream, ParseTreeWalker, InputStream
from conjuntosLexer import conjuntosLexer
from conjuntosParser import conjuntosParser
from conjuntosListener import conjuntosListener
import antlr4

class RealListener(conjuntosListener):

    def IsInMyList(self,array,number):
        if (number in array):
            return True
        else:
            return False
    
    def calculateAverage(self,array):
        return sum(array) / len(array)

    def __init__(self):
        self.variables = {}


    def enterProgram(self, ctx:conjuntosParser.ProgramContext):
        for node in ctx.children:
            self.visitStatement(node)


    def exitProgram(self, ctx:conjuntosParser.ProgramContext):
        print('Program ended. :)')


    def visitStatement(self, ctx:conjuntosParser.StatementContext):
        for node in ctx.children:
            if type(node) == conjuntosParser.ExpressionContext:
                result = self.visitExpression(node)
                print(result)
            
            elif type(node) == conjuntosParser.AssignStatementContext:
                self.visitAssignStatement(node)

            elif type(node) == antlr4.tree.Tree.TerminalNodeImpl:   # VARNAME
                result = self.variables[node.symbol.text]
                print(node.symbol.text + ' = ' + str(result))

            elif type(node) == conjuntosParser.BooleanExpressionContext:
                result = self.visitBooleanExpresion(node)
                print(result)

            elif type(node) == conjuntosParser.IfStatementContext:
                condition = self.visitBooleanExpresion(node.children[1])
                if condition:
                    i = 3
                    while i < len(node.children) - 1 and type(node.children[i]) != antlr4.tree.Tree.TerminalNodeImpl:
                        self.visitStatement(node.children[i])
                        i += 1
                else:
                    i = 3
                    while i < len(node.children) - 1 and type(node.children[i]) != antlr4.tree.Tree.TerminalNodeImpl:
                        i += 1
                        # sale del while cuando encuentra el ELSE
                    
                    i += 1      # Para saltear el ELSE
                    while i < len(node.children) - 1:
                        self.visitStatement(node.children[i])
                        i += 1

            elif type(node) == conjuntosParser.SetNumberContext:
                self.visitSetNumber(node)

            elif type(node) == conjuntosParser.SetConjuntosContext:
                self.setVisitSetConjuntos(node)
                

            elif type(node) == conjuntosParser.WhileStatementContext:
                self.visitWhileStatement(node)


    def visitWhileStatement(self, ctx:conjuntosParser.WhileStatementContext):
        
        while self.visitBooleanExpresion(ctx.children[1]):
            print('en el while!!!')
            i = 3
            while i < len(ctx.children) - 1 and type(ctx.children[i]) != antlr4.tree.Tree.TerminalNodeImpl:
                self.visitStatement(ctx.children[i])
                i += 1


    def visitIfStatement(self, ctx:conjuntosParser.IfStatementContext):
        pass



    # Ok
    def visitBooleanExpresion(self, ctx:conjuntosParser.BooleanExpressionContext):

        print("BooleanExpression")
        if ctx.op.text == 'and':
            be_left = self.visitBooleanExpresion(ctx.children[0])
            be_right = self.visitBooleanExpresion(ctx.children[2])
            return be_left and be_right

        if ctx.op.text == 'or':
            be_left = self.visitBooleanExpresion(ctx.children[0])
            be_right = self.visitBooleanExpresion(ctx.children[2])
            return be_left or be_right

        left =  self.visitOperand(ctx.children[0])
        right = self.visitOperand(ctx.children[2])
        op = ctx.children[1].symbol.text

        return eval(str(left) + op + str(right))

    # Ok
    def visitOperand(self, ctx:conjuntosParser.OperandContext):
        
        print("OPERAND")
        node = ctx.children[0]
        if type(node) == conjuntosParser.ExpressionContext:
            return self.visitExpression(node)

        if type(node) == antlr4.tree.Tree.TerminalNodeImpl:
            return self.variables[node.symbol.text]
        
        return ''   # No deberia pasar

    # Ok
    def visitAssignStatement(self, ctx:conjuntosParser.AssignStatementContext):
        
        # En name guardo el nombre de a variable
        name = ctx.children[0].symbol.text
        # En value guardo el conjunto de numeros (setNumber)
        value = None 

        print("ASSIGN")
        # Tengo que validad 2 condiciones:
        # VARNAME '=' expression | VARNAME '=' setNumber
        # Lo valido en la posición 2, ya que todos comparten VARNAME =
    
        if type(ctx.children[2]) == conjuntosParser.ExpressionContext:
            value = self.visitExpression(ctx.children[2])
        
        elif type(ctx.children[2]) == conjuntosParser.SetNumberContext:
            value = self.visitSetNumber(ctx.children[2])
        
        elif type(ctx.children[2]) == conjuntosParser.SetConjuntosContext:
            value = self.setVisitSetConjuntos(ctx.children[2])

        self.variables[name]=value

    def setVisitSetConjuntos(self,ctx:conjuntosParser.SetConjuntosContext):
        
        # Si es de tipo intersection
        if type(ctx.children[0]) == conjuntosParser.SetIntersectionContext:
            return self.setVisitSetIntersection(ctx.children[0])
        elif type(ctx.children[0]) == conjuntosParser.SetUnionContext:
            return self.setVisitUnion(ctx.children[0])
        elif type(ctx.children[0]) == conjuntosParser.SetComplementContext:
            return self.setVisitComplement(ctx.children[0])
        elif type(ctx.children[0]) == conjuntosParser.SetDifferenceContext:
            return self.setVisitDifference(ctx.children[0])
        elif type(ctx.children[0]) == conjuntosParser.SetBelongsContext:
            return self.setVisitBelongs(ctx.children[0])
        elif type(ctx.children[0]) == conjuntosParser.SetSumContext:
            return self.setVisitSum(ctx.children[0])
        elif type(ctx.children[0]) == conjuntosParser.SetAverageContext:
            return self.setVisitAverage(ctx.children[0])
        elif type(ctx.children[0]) == conjuntosParser.SetLengthContext:
            return self.setVisitLength(ctx.children[0])

    def setVisitSetIntersection(self,ctx:conjuntosParser.SetIntersectionContext):
        interesection = set(self.variables[ctx.name.text]) & set(self.variables[ctx.name2.text])

        return list(interesection)

    def setVisitUnion(self,ctx:conjuntosParser.SetUnionContext):
        union = set(self.variables[ctx.name.text]).union(self.variables[ctx.name2.text])

        return list(union)
    
    def setVisitComplement(self,ctx:conjuntosParser.SetComplementContext):
        complement = set(self.variables[ctx.name2.text]) - set(self.variables[ctx.name.text])

        return list(complement)

    def setVisitDifference(self,ctx:conjuntosParser.SetDifferenceContext):
        difference = set(self.variables[ctx.name.text]).symmetric_difference(set(self.variables[ctx.name2.text]))

        return list(difference)

    def setVisitBelongs(self,ctx:conjuntosParser.SetBelongsContext):
        belongs = self.IsInMyList(self.variables[ctx.name.text],int(ctx.name2.text))
        return belongs
    
    def setVisitSum(self,ctx:conjuntosParser.SetSumContext):
        result = sum(set(self.variables[ctx.name.text]))
        return result
    
    def setVisitAverage(self,ctx:conjuntosParser.SetAverageContext):
        average = self.calculateAverage(set(self.variables[ctx.name.text]))
        return average
    
    def setVisitLength(self,ctx:conjuntosParser.SetLengthContext):
        length = len(set(self.variables[ctx.name.text]))
        return length

    def visitSetNumber(self, ctx:conjuntosParser.SetNumberContext):
        
        print("VisitSetNumber")
        # variable que guarda el conjunto de numeros
        elements = []
        start_Number = int(ctx.start.text)
        end_Number = int(ctx.end.text)
             

        for i in range(start_Number, end_Number + 1):
            elements.append(i)
        print("---Conjunto---")
        for i,val in enumerate(elements):
            print("[{}] = {}".format(i,val))
            
        return elements

    # Ok
    def visitExpression(self, ctx:conjuntosParser.ExpressionContext):
        
        print("EXPRESSION")
        # Valido si es un termino
        if type(ctx.children[0]) == conjuntosParser.TermContext:
            # Es un Term
            return self.visitTerm(ctx.children[0])
        
        # Valido si es una Expression + - Term
        sign = 1
        if ctx.children[1].symbol.text == '-':
            sign = -1

        value1 = self.visitExpression(ctx.children[0])
        value2 = self.visitTerm(ctx.children[2])

        return value1 + sign * value2

    # Ok
    def visitTerm(self, ctx:conjuntosParser.TermContext):
        
        print("TERM")
        
        # Valida el término de la izquierda
        if type(ctx.children[0]) == conjuntosParser.FactorContext:
            # Es un factor
            return self.visitFactor(ctx.children[0])

        # Es un term * / factor
        value1 = self.visitTerm(ctx.children[0])
        value2 = self.visitFactor(ctx.children[2])

        # Valida el término de la derecha
        if ctx.children[1].symbol.text == '*':
            return value1 * value2

        return value1 / value2

    # Ok
    def visitFactor(self, ctx:conjuntosParser.FactorContext):
        
        print("FACTOR");

        # Si es un numero
        if ctx.n != None:
            return int(ctx.n.text)

        # Si es una varible
        if ctx.vn != None:                          
            return self.variables[ctx.vn.text]

        # Si es una expresion
        if ctx.children[0].symbol.text != '(':
            nodo = ctx.children[0]
            return int(nodo.symbol.text)

        return self.visitExpression(ctx.children[1])
        

        

def main():
  
    #program = "56 == 46 \n"
    #program += "6 == 3 \n"
    ##### PRUEBA CONJUNTOS #####
    program = "a=set[1 6]"
    program+= "b=set[2 8]"
    #program+= "c = b.intersection(a)" # Cuando voy a visitAssignStatement
                                     # en [2] lo detecta como expression y no como setIntersection,
                                    # por lo que no visita el metodo visitSetInteresection

    
    program+= "d = a.union(b)"
    program+= "e = a.complement(b)"
    program+= "f = a.difference(b)"
    program+= "g = a.belongs(46)"
    program+= "h = a.sum()"
    program+= "i = a.average()"
    program+= "j = a.length()"
    
    #program += "b = 54 \n"
    #program += " a == b \n"
    #program += "c = (23 *2-4)*5+34-16/4  \n"
    #program += "c <= 241  \n"
    #program += "b   "
    #program += "3+4-9*5  \n "
    #program += "a   \n"
    #program += "if 1==2 or a == 37 and 4 == 2+2*3  then " 
    #program += "   a = 666 "

    #program += "   if b == 54 then"
    #program += "      z = 5568558 "
    #program += "   fi  \n" 

    #program += "   x = 999 "
    #program += "else "
    #program += "   a =1234 "
    #program += "   y = 564 "
    #program += "fi  \n" 



    #program = " a = 0 "
    #program += "while a < 10 do "
    #program += "    a = a + 1 "
    #program += "done "
    #program += "a"


    input = InputStream(program)
    lexer = conjuntosLexer(input)
    stream = CommonTokenStream(lexer)
    parser = conjuntosParser(stream)

    tree = parser.program()
    
    listener = RealListener()
    walker = ParseTreeWalker()
    walker.walk(listener, tree)
    
    print(listener.variables)
    
if __name__ == '__main__':
    main()


