grammar Aritmetica;

statement:
    e=expression                {print("statement: {}".format($e.value)) }
                                
    ;

expression returns [int value]: 
                                
    t=term                      {$value = $t.value}
                                {print("Expression {}".format($t.value))}
    | e=expression '+' t=term   {$value = $e.value + $t.value }
                               
    ;

term returns [int value]:   
    e=factor                    {$value = $e.value }
                                {print("Term: {}".format($e.value))}
    | t=term '*' e=factor       {$value = $t.value * $e.value }
    ;

factor returns [int value]: 
    NUMBER                      {$value = int($NUMBER.text)}
                                {print("factor: {}".format($NUMBER.text))}

    | '(' e=expression ')'      {$value = $e.value }
                               
    ;

NUMBER : DIGIT+;
DIGIT  : [0-9];
WS : [ \r\n\t] -> skip; 
