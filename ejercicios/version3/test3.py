from antlr4 import CommonTokenStream, ParseTreeWalker, InputStream
from AritmeticaLexer import AritmeticaLexer
from AritmeticaParser import AritmeticaParser
from AritmeticaListener import AritmeticaListener

class muestroListener(AritmeticaListener):

    step=0

    def enterStatement(self,ctx:AritmeticaParser.StatementContext):
        print("STATEMENT: {}".format(str(self.step)))
        self.step+=1

    def enterExpression(self,ctx:AritmeticaParser.ExpressionContext):
        print("EXPRESSION: {}".format(str(self.step)))
        self.step+=1

    def enterTerm(self,ctx:AritmeticaParser.TermContext):
        print("TERMINO: {}".format(str(self.step)))
        self.step+=1

     
    def enterFactor(self,ctx:AritmeticaParser.FactorContext):
        print("FACTOR: {}".format(str(self.step)))
        self.step+=1

def main():
    input = InputStream("23*5")
    lexer = AritmeticaLexer(input)
    stream = CommonTokenStream(lexer)
    parser = AritmeticaParser(stream)
    tree = parser.statement();
    
    #parser.statement()                
 
    printer=muestroListener();
    walker=ParseTreeWalker();

    walker.walk(printer,tree);

if __name__ == '__main__':
    main()
