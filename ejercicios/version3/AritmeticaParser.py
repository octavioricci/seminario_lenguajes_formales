# Generated from Aritmetica.g4 by ANTLR 4.7.1
# encoding: utf-8
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys

def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\t")
        buf.write("\66\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\3\2\3\2\3\2\3\3\3")
        buf.write("\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\7\3\30\n\3\f\3\16\3")
        buf.write("\33\13\3\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\7\4\'")
        buf.write("\n\4\f\4\16\4*\13\4\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\5")
        buf.write("\5\64\n\5\3\5\2\4\4\6\6\2\4\6\b\2\2\2\64\2\n\3\2\2\2\4")
        buf.write("\r\3\2\2\2\6\34\3\2\2\2\b\63\3\2\2\2\n\13\5\4\3\2\13\f")
        buf.write("\b\2\1\2\f\3\3\2\2\2\r\16\b\3\1\2\16\17\5\6\4\2\17\20")
        buf.write("\b\3\1\2\20\21\b\3\1\2\21\31\3\2\2\2\22\23\f\3\2\2\23")
        buf.write("\24\7\3\2\2\24\25\5\6\4\2\25\26\b\3\1\2\26\30\3\2\2\2")
        buf.write("\27\22\3\2\2\2\30\33\3\2\2\2\31\27\3\2\2\2\31\32\3\2\2")
        buf.write("\2\32\5\3\2\2\2\33\31\3\2\2\2\34\35\b\4\1\2\35\36\5\b")
        buf.write("\5\2\36\37\b\4\1\2\37 \b\4\1\2 (\3\2\2\2!\"\f\3\2\2\"")
        buf.write("#\7\4\2\2#$\5\b\5\2$%\b\4\1\2%\'\3\2\2\2&!\3\2\2\2\'*")
        buf.write("\3\2\2\2(&\3\2\2\2()\3\2\2\2)\7\3\2\2\2*(\3\2\2\2+,\7")
        buf.write("\7\2\2,-\b\5\1\2-\64\b\5\1\2./\7\5\2\2/\60\5\4\3\2\60")
        buf.write("\61\7\6\2\2\61\62\b\5\1\2\62\64\3\2\2\2\63+\3\2\2\2\63")
        buf.write(".\3\2\2\2\64\t\3\2\2\2\5\31(\63")
        return buf.getvalue()


class AritmeticaParser ( Parser ):

    grammarFileName = "Aritmetica.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "'+'", "'*'", "'('", "')'" ]

    symbolicNames = [ "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "NUMBER", "DIGIT", "WS" ]

    RULE_statement = 0
    RULE_expression = 1
    RULE_term = 2
    RULE_factor = 3

    ruleNames =  [ "statement", "expression", "term", "factor" ]

    EOF = Token.EOF
    T__0=1
    T__1=2
    T__2=3
    T__3=4
    NUMBER=5
    DIGIT=6
    WS=7

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7.1")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None



    class StatementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.e = None # ExpressionContext

        def expression(self):
            return self.getTypedRuleContext(AritmeticaParser.ExpressionContext,0)


        def getRuleIndex(self):
            return AritmeticaParser.RULE_statement

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterStatement" ):
                listener.enterStatement(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitStatement" ):
                listener.exitStatement(self)




    def statement(self):

        localctx = AritmeticaParser.StatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_statement)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 8
            localctx.e = self.expression(0)
            print("statement: {}".format(localctx.e.value)) 
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class ExpressionContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.value = None
            self.e = None # ExpressionContext
            self.t = None # TermContext

        def term(self):
            return self.getTypedRuleContext(AritmeticaParser.TermContext,0)


        def expression(self):
            return self.getTypedRuleContext(AritmeticaParser.ExpressionContext,0)


        def getRuleIndex(self):
            return AritmeticaParser.RULE_expression

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterExpression" ):
                listener.enterExpression(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitExpression" ):
                listener.exitExpression(self)



    def expression(self, _p:int=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = AritmeticaParser.ExpressionContext(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 2
        self.enterRecursionRule(localctx, 2, self.RULE_expression, _p)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 12
            localctx.t = self.term(0)
            localctx.value = localctx.t.value
            print("Expression {}".format(localctx.t.value))
            self._ctx.stop = self._input.LT(-1)
            self.state = 23
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,0,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    localctx = AritmeticaParser.ExpressionContext(self, _parentctx, _parentState)
                    localctx.e = _prevctx
                    self.pushNewRecursionContext(localctx, _startState, self.RULE_expression)
                    self.state = 16
                    if not self.precpred(self._ctx, 1):
                        from antlr4.error.Errors import FailedPredicateException
                        raise FailedPredicateException(self, "self.precpred(self._ctx, 1)")
                    self.state = 17
                    self.match(AritmeticaParser.T__0)
                    self.state = 18
                    localctx.t = self.term(0)
                    localctx.value = localctx.e.value + localctx.t.value  
                self.state = 25
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,0,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx

    class TermContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.value = None
            self.t = None # TermContext
            self.e = None # FactorContext

        def factor(self):
            return self.getTypedRuleContext(AritmeticaParser.FactorContext,0)


        def term(self):
            return self.getTypedRuleContext(AritmeticaParser.TermContext,0)


        def getRuleIndex(self):
            return AritmeticaParser.RULE_term

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTerm" ):
                listener.enterTerm(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTerm" ):
                listener.exitTerm(self)



    def term(self, _p:int=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = AritmeticaParser.TermContext(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 4
        self.enterRecursionRule(localctx, 4, self.RULE_term, _p)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 27
            localctx.e = self.factor()
            localctx.value = localctx.e.value 
            print("Term: {}".format(localctx.e.value))
            self._ctx.stop = self._input.LT(-1)
            self.state = 38
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,1,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    localctx = AritmeticaParser.TermContext(self, _parentctx, _parentState)
                    localctx.t = _prevctx
                    self.pushNewRecursionContext(localctx, _startState, self.RULE_term)
                    self.state = 31
                    if not self.precpred(self._ctx, 1):
                        from antlr4.error.Errors import FailedPredicateException
                        raise FailedPredicateException(self, "self.precpred(self._ctx, 1)")
                    self.state = 32
                    self.match(AritmeticaParser.T__1)
                    self.state = 33
                    localctx.e = self.factor()
                    localctx.value = localctx.t.value * localctx.e.value  
                self.state = 40
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,1,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx

    class FactorContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.value = None
            self._NUMBER = None # Token
            self.e = None # ExpressionContext

        def NUMBER(self):
            return self.getToken(AritmeticaParser.NUMBER, 0)

        def expression(self):
            return self.getTypedRuleContext(AritmeticaParser.ExpressionContext,0)


        def getRuleIndex(self):
            return AritmeticaParser.RULE_factor

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterFactor" ):
                listener.enterFactor(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitFactor" ):
                listener.exitFactor(self)




    def factor(self):

        localctx = AritmeticaParser.FactorContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_factor)
        try:
            self.state = 49
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [AritmeticaParser.NUMBER]:
                self.enterOuterAlt(localctx, 1)
                self.state = 41
                localctx._NUMBER = self.match(AritmeticaParser.NUMBER)
                localctx.value = int((None if localctx._NUMBER is None else localctx._NUMBER.text))
                print("factor: {}".format((None if localctx._NUMBER is None else localctx._NUMBER.text)))
                pass
            elif token in [AritmeticaParser.T__2]:
                self.enterOuterAlt(localctx, 2)
                self.state = 44
                self.match(AritmeticaParser.T__2)
                self.state = 45
                localctx.e = self.expression(0)
                self.state = 46
                self.match(AritmeticaParser.T__3)
                localctx.value = localctx.e.value 
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx



    def sempred(self, localctx:RuleContext, ruleIndex:int, predIndex:int):
        if self._predicates == None:
            self._predicates = dict()
        self._predicates[1] = self.expression_sempred
        self._predicates[2] = self.term_sempred
        pred = self._predicates.get(ruleIndex, None)
        if pred is None:
            raise Exception("No predicate with index:" + str(ruleIndex))
        else:
            return pred(localctx, predIndex)

    def expression_sempred(self, localctx:ExpressionContext, predIndex:int):
            if predIndex == 0:
                return self.precpred(self._ctx, 1)
         

    def term_sempred(self, localctx:TermContext, predIndex:int):
            if predIndex == 1:
                return self.precpred(self._ctx, 1)
         




