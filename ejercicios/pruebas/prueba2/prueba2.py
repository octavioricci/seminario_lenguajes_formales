from antlr4 import CommonTokenStream,ParseTreeWalker,FileStream,InputStream
from prueba2Lexer import prueba2Lexer
from prueba2Parser import prueba2Parser
from prueba2Listener import prueba2Listener

class muestroListener(prueba2Listener):
    
    step=0

    def enterOperation(self,ctx:prueba2Parser.OperationContext):
        print("Enter Operation " + str(self.step));
        self.step+=1;

    def enterAssign(self,ctx):
        print("Enter Assignment " + str(self.step));
        self.step+=1;

        #print("%s" % ctx.children[0].symbol.text);
        print("{}".format(ctx.children[0].symbol.text),format(ctx.children[1].symbol.text),format(ctx.children[2].symbol.text));
        #print("{} {} {}".format(ctx.children[0].symbol.text,ctx.children[1].symbol.text,ctx.children[2].symbol.text));

    

def main():
    print("MAIN");
    ##program = ("a=987");
    program="2 - 2";
    input = InputStream(program);
    lexer = prueba2Lexer(input);
    stream = CommonTokenStream(lexer);
    parser = prueba2Parser(stream);

    ##tree = parser.assign();
    tree = parser.operation();
    printer=muestroListener();
    walker=ParseTreeWalker();

    walker.walk(printer,tree);



if __name__ == '__main__':
    main()