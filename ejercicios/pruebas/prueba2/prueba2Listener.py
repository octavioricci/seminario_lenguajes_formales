# Generated from prueba2.g4 by ANTLR 4.7.1
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .prueba2Parser import prueba2Parser
else:
    from prueba2Parser import prueba2Parser

# This class defines a complete listener for a parse tree produced by prueba2Parser.
class prueba2Listener(ParseTreeListener):

    # Enter a parse tree produced by prueba2Parser#operation.
    def enterOperation(self, ctx:prueba2Parser.OperationContext):
        pass

    # Exit a parse tree produced by prueba2Parser#operation.
    def exitOperation(self, ctx:prueba2Parser.OperationContext):
        pass


    # Enter a parse tree produced by prueba2Parser#assign.
    def enterAssign(self, ctx:prueba2Parser.AssignContext):
        pass

    # Exit a parse tree produced by prueba2Parser#assign.
    def exitAssign(self, ctx:prueba2Parser.AssignContext):
        pass


