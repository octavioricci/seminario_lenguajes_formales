# Generated from prueba2.g4 by ANTLR 4.7.1
# encoding: utf-8
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys

def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\t")
        buf.write("\23\4\2\t\2\4\3\t\3\3\2\3\2\3\2\3\2\3\2\3\2\5\2\r\n\2")
        buf.write("\3\3\3\3\3\3\3\3\3\3\2\2\4\2\4\2\2\2\21\2\f\3\2\2\2\4")
        buf.write("\16\3\2\2\2\6\7\7\7\2\2\7\b\7\3\2\2\b\r\7\7\2\2\t\n\7")
        buf.write("\7\2\2\n\13\7\4\2\2\13\r\7\7\2\2\f\6\3\2\2\2\f\t\3\2\2")
        buf.write("\2\r\3\3\2\2\2\16\17\7\6\2\2\17\20\7\5\2\2\20\21\7\7\2")
        buf.write("\2\21\5\3\2\2\2\3\f")
        return buf.getvalue()


class prueba2Parser ( Parser ):

    grammarFileName = "prueba2.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "'+'", "'-'", "'='" ]

    symbolicNames = [ "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "VARNAME", "NUMBER", "DIGIT", "WHITESPACE" ]

    RULE_operation = 0
    RULE_assign = 1

    ruleNames =  [ "operation", "assign" ]

    EOF = Token.EOF
    T__0=1
    T__1=2
    T__2=3
    VARNAME=4
    NUMBER=5
    DIGIT=6
    WHITESPACE=7

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7.1")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None



    class OperationContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def NUMBER(self, i:int=None):
            if i is None:
                return self.getTokens(prueba2Parser.NUMBER)
            else:
                return self.getToken(prueba2Parser.NUMBER, i)

        def getRuleIndex(self):
            return prueba2Parser.RULE_operation

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterOperation" ):
                listener.enterOperation(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitOperation" ):
                listener.exitOperation(self)




    def operation(self):

        localctx = prueba2Parser.OperationContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_operation)
        try:
            self.state = 10
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,0,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 4
                self.match(prueba2Parser.NUMBER)
                self.state = 5
                self.match(prueba2Parser.T__0)
                self.state = 6
                self.match(prueba2Parser.NUMBER)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 7
                self.match(prueba2Parser.NUMBER)
                self.state = 8
                self.match(prueba2Parser.T__1)
                self.state = 9
                self.match(prueba2Parser.NUMBER)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class AssignContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def VARNAME(self):
            return self.getToken(prueba2Parser.VARNAME, 0)

        def NUMBER(self):
            return self.getToken(prueba2Parser.NUMBER, 0)

        def getRuleIndex(self):
            return prueba2Parser.RULE_assign

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAssign" ):
                listener.enterAssign(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAssign" ):
                listener.exitAssign(self)




    def assign(self):

        localctx = prueba2Parser.AssignContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_assign)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 12
            self.match(prueba2Parser.VARNAME)
            self.state = 13
            self.match(prueba2Parser.T__2)
            self.state = 14
            self.match(prueba2Parser.NUMBER)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx





