# Generated from prueba1.g4 by ANTLR 4.7.1
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys


def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2\5")
        buf.write("\27\b\1\4\2\t\2\4\3\t\3\4\4\t\4\3\2\3\2\3\2\3\2\3\2\3")
        buf.write("\3\6\3\20\n\3\r\3\16\3\21\3\4\3\4\3\4\3\4\2\2\5\3\3\5")
        buf.write("\4\7\5\3\2\4\3\2C\\\5\2\13\f\17\17\"\"\2\27\2\3\3\2\2")
        buf.write("\2\2\5\3\2\2\2\2\7\3\2\2\2\3\t\3\2\2\2\5\17\3\2\2\2\7")
        buf.write("\23\3\2\2\2\t\n\7j\2\2\n\13\7q\2\2\13\f\7n\2\2\f\r\7c")
        buf.write("\2\2\r\4\3\2\2\2\16\20\t\2\2\2\17\16\3\2\2\2\20\21\3\2")
        buf.write("\2\2\21\17\3\2\2\2\21\22\3\2\2\2\22\6\3\2\2\2\23\24\t")
        buf.write("\3\2\2\24\25\3\2\2\2\25\26\b\4\2\2\26\b\3\2\2\2\4\2\21")
        buf.write("\3\b\2\2")
        return buf.getvalue()


class prueba1Lexer(Lexer):

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    T__0 = 1
    UPPERCASE = 2
    WHITESPACE = 3

    channelNames = [ u"DEFAULT_TOKEN_CHANNEL", u"HIDDEN" ]

    modeNames = [ "DEFAULT_MODE" ]

    literalNames = [ "<INVALID>",
            "'hola'" ]

    symbolicNames = [ "<INVALID>",
            "UPPERCASE", "WHITESPACE" ]

    ruleNames = [ "T__0", "UPPERCASE", "WHITESPACE" ]

    grammarFileName = "prueba1.g4"

    def __init__(self, input=None, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7.1")
        self._interp = LexerATNSimulator(self, self.atn, self.decisionsToDFA, PredictionContextCache())
        self._actions = None
        self._predicates = None


