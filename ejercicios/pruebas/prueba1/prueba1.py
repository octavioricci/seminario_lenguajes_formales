
from antlr4 import *
from prueba1Lexer import prueba1Lexer
from prueba1Parser import prueba1Parser
from prueba1Listener import prueba1Listener
import sys

class PrintListener(prueba1Listener):
    
    def enterExpression(self,ctx):
        print("Enter Expression")
        print("%s" % ctx.UPPERCASE());
    
           

def main():
        print("--------MAIN-----------")
        #lexer=prueba1Lexer(StdinStream())
        input=InputStream('hola OCTA')
        lexer=prueba1Lexer(input)
        stream=CommonTokenStream(lexer)
        parser=prueba1Parser(stream)
        tree=parser.expression()
        printer=PrintListener()
        walker=ParseTreeWalker()

        walker.walk(printer,tree)

        print("-----------------------")

if __name__ == '__main__':
    main()




