# Generated from prueba1.g4 by ANTLR 4.7.1
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .prueba1Parser import prueba1Parser
else:
    from prueba1Parser import prueba1Parser

# This class defines a complete listener for a parse tree produced by prueba1Parser.
class prueba1Listener(ParseTreeListener):

    # Enter a parse tree produced by prueba1Parser#expression.
    def enterExpression(self, ctx:prueba1Parser.ExpressionContext):
        pass

    # Exit a parse tree produced by prueba1Parser#expression.
    def exitExpression(self, ctx:prueba1Parser.ExpressionContext):
        pass


