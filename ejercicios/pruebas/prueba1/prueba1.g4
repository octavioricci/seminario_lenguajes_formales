grammar prueba1;

expression: 'hola' UPPERCASE;

UPPERCASE: [A-Z]+;

//LOWERCASE: [a-z]+;

WHITESPACE: [ \r\n\t] -> skip;
