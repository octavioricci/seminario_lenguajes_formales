grammar prueba3;
// REGLA

operation returns [int value]: 
    
NUMBER '+' NUMBER | NUMBER '-' NUMBER;

assign: VARNAME '=' NUMBER;

VARNAME: [a-z]+;

NUMBER: [0-9]+;

DIGIT: [0-9];

WHITESPACE: [ \r\t\n] -> skip;